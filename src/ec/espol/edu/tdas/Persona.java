/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.espol.edu.tdas;

/**
 *
 * @author diego
 */
public class Persona {
    int registro;
    String tip_movi,tip_naci,via_tran,pro_jefm,can_jefm,ani_movi,mes_movi,dia_movi,sex_migr,ani_nacm,ocu_migr,cla_migr,mot_viam,nac_migr,pais_prod,pais_res,lug_pro,edad,cont_prod,cont_res,cont_nac,subcont_prod,subcont_nac;

    public Persona(int registro, String tip_movi, String tip_naci, String via_tran, String pro_jefm, String can_jefm, String ani_movi, String mes_movi, String dia_movi, String sex_migr, String ani_nacm, String ocu_migr, String cla_migr, String mot_viam, String nac_migr, String pais_prod, String pais_res, String lug_pro, String edad, String cont_prod, String cont_res, String cont_nac, String subcont_prod, String subcont_nac) {
        this.registro = registro;
        this.tip_movi = tip_movi;
        this.tip_naci = tip_naci;
        this.via_tran = via_tran;
        this.pro_jefm = pro_jefm;
        this.can_jefm = can_jefm;
        this.ani_movi = ani_movi;
        this.mes_movi = mes_movi;
        this.dia_movi = dia_movi;
        this.sex_migr = sex_migr;
        this.ani_nacm = ani_nacm;
        this.ocu_migr = ocu_migr;
        this.cla_migr = cla_migr;
        this.mot_viam = mot_viam;   
        this.nac_migr = nac_migr;
        this.pais_prod = pais_prod;
        this.pais_res = pais_res;
        this.lug_pro = lug_pro;
        this.edad = edad;
        this.cont_prod = cont_prod;
        this.cont_res = cont_res;
        this.cont_nac = cont_nac;
        this.subcont_prod = subcont_prod;
        this.subcont_nac = subcont_nac;
    }

   

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }

    public String getTip_movi() {
        return tip_movi;
    }

    public void setTip_movi(String tip_movi) {
        this.tip_movi = tip_movi;
    }

    public String getTip_naci() {
        return tip_naci;
    }

    public void setTip_naci(String tip_naci) {
        this.tip_naci = tip_naci;
    }

    public String getVia_tran() {
        return via_tran;
    }

    public void setVia_tran(String via_tran) {
        this.via_tran = via_tran;
    }

    public String getPro_jefm() {
        return pro_jefm;
    }

    public void setPro_jefm(String pro_jefm) {
        this.pro_jefm = pro_jefm;
    }

    public String getCan_jefm() {
        return can_jefm;
    }

    public void setCan_jefm(String can_jefm) {
        this.can_jefm = can_jefm;
    }

    public String getAni_movi() {
        return ani_movi;
    }

    public void setAni_movi(String ani_movi) {
        this.ani_movi = ani_movi;
    }

    public String getMes_movi() {
        return mes_movi;
    }

    public void setMes_movi(String mes_movi) {
        this.mes_movi = mes_movi;
    }

    public String getDia_movi() {
        return dia_movi;
    }

    public void setDia_movi(String dia_movi) {
        this.dia_movi = dia_movi;
    }

    public String getSex_migr() {
        return sex_migr;
    }

    public void setSex_migr(String sex_migr) {
        this.sex_migr = sex_migr;
    }

    public String getAni_nacm() {
        return ani_nacm;
    }

    public void setAni_nacm(String ani_nacm) {
        this.ani_nacm = ani_nacm;
    }

    public String getOcu_migr() {
        return ocu_migr;
    }

    public void setOcu_migr(String ocu_migr) {
        this.ocu_migr = ocu_migr;
    }

    public String getCla_migr() {
        return cla_migr;
    }

    public void setCla_migr(String cla_migr) {
        this.cla_migr = cla_migr;
    }

    public String getMot_viam() {
        return mot_viam;
    }

    public void setMot_viam(String mot_viam) {
        this.mot_viam = mot_viam;
    }

    public String getNac_migr() {
        return nac_migr;
    }

    public void setNac_migr(String nac_migr) {
        this.nac_migr = nac_migr;
    }

    public String getPais_prod() {
        return pais_prod;
    }

    public void setPais_prod(String pais_prod) {
        this.pais_prod = pais_prod;
    }

    public String getPais_res() {
        return pais_res;
    }

    public void setPais_res(String pais_res) {
        this.pais_res = pais_res;
    }

    public String getLug_pro() {
        return lug_pro;
    }

    public void setLug_pro(String lug_pro) {
        this.lug_pro = lug_pro;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getCont_prod() {
        return cont_prod;
    }

    public void setCont_prod(String cont_prod) {
        this.cont_prod = cont_prod;
    }

    public String getCont_res() {
        return cont_res;
    }

    public void setCont_res(String cont_res) {
        this.cont_res = cont_res;
    }

    public String getCont_nac() {
        return cont_nac;
    }

    public void setCont_nac(String cont_nac) {
        this.cont_nac = cont_nac;
    }

    public String getSubcont_prod() {
        return subcont_prod;
    }

    public void setSubcont_prod(String subcont_prod) {
        this.subcont_prod = subcont_prod;
    }

    public String getSubcont_nac() {
        return subcont_nac;
    }

    public void setSubcont_nac(String subcont_nac) {
        this.subcont_nac = subcont_nac;
    }
    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append("[");
        sb.append(this.getRegistro()); sb.append(",");
        sb.append(this.getTip_movi());sb.append(",");
        sb.append(this.getTip_naci());sb.append(",");
        sb.append(this.getVia_tran());sb.append(",");
        sb.append(this.getPro_jefm());sb.append(",");
        sb.append(this.getCan_jefm());sb.append(",");
        sb.append(this.getAni_movi());sb.append(",");
        sb.append(this.getMes_movi());sb.append(",");
        sb.append(this.getDia_movi());sb.append(",");
        sb.append(this.getSex_migr());sb.append(",");
        sb.append(this.getAni_nacm());sb.append(",");
        sb.append(this.getOcu_migr());sb.append(",");
        sb.append(this.getCla_migr());sb.append(",");
        sb.append(this.getMot_viam());   sb.append(",");
        sb.append(this.getNac_migr());sb.append(",");
        sb.append(this.getPais_prod());sb.append(",");
        sb.append(this.getPais_res());sb.append(",");
        sb.append(this.getLug_pro());sb.append(",");
        sb.append(this.getEdad());sb.append(",");
        sb.append(this.getCont_prod());sb.append(",");
        sb.append(this.getCont_res ());sb.append(",");
        sb.append(this.getCont_nac ());sb.append(",");
        sb.append(this.getSubcont_prod());sb.append(",");
        sb.append(this.getSubcont_nac()) ;sb.append(",");
                    
                sb.append("]");
            
        
        return sb.toString();
    
}}
