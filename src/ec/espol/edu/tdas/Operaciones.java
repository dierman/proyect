/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.espol.edu.tdas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author diego
 */
public class Operaciones {
     
    FileReader fr=null;
    BufferedReader br=null;

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
    public int ced_auto(){
        this.setC(c++);
        return this.getC();
    }
    int c=1;    
    
    public DoublyLinkedList<Persona> leerArchivo(DoublyLinkedList<Persona> lista){
        String linea;
        
   try{
   
   File archivo = new File ("C:\\Users\\diego\\Documents\\NetBeansProjects\\Proyect\\src\\ec\\espol\\edu\\multi\\migraciones.csv");
    FileReader fr = new FileReader (archivo);
    BufferedReader br = new BufferedReader(fr);
    
         while((linea=br.readLine())!=null){
             String[] l=linea.split(";");
             Persona op=new Persona(c,l[0],l[1],l[2],l[3],l[4],l[5],l[6],l[7],l[8],l[9],l[10],l[11],l[12],l[13],l[14],l[15],l[16],l[17],l[18],l[19],l[20],l[21],l[22]);
             lista.addLast(op);
             c++;
            } 
       
   }catch (IOException E){
       System.out.println("se murio todo :'v");
       
   }finally{
       try {
   if (null != fr) {
    fr.close();
   }
   if (null != br) {
    br.close();
   }
       
   } catch (IOException exFinally){
       System.out.println("el archivo sigue vacio");
   }
       lista.removeFirst();
   return lista;
        
    }}

public boolean agregarPersona(DoublyLinkedList<Persona> lista,Persona per){
    lista.addLast(per);
    System.out.println(lista.getLast().toString());
    return true;
}

public boolean eliminarPersona(DoublyLinkedList<Persona> lista,int c){
    return lista.remove(this.busquedaCedula(lista, c));
    
}
public boolean modificarPersona(DoublyLinkedList<Persona> list, int c, Persona mod){
    int indice=this.busquedaCedula(list, c);
    Persona op=list.getIndice(this.busquedaCedula(list, c));
    Persona op2=mod;
        if(op2.getAni_movi()!=null){
            op.setAni_movi(op2.getAni_movi());}
        if(op2.getAni_nacm()!=null){
            op.setAni_nacm(op2.getAni_nacm());
        }
        if(op2.getCan_jefm()!=null){
            op.setCan_jefm(op2.getCan_jefm());
        }
        if(op2.getCla_migr()!=null){
            op.setCla_migr(op2.getCla_migr());
        }
        if(op2.getCont_nac()!=null){
            op.setCont_nac(op2.getCont_nac());
        }
        if(op2.getCont_prod()!=null){
            op.setCont_prod(op2.getCont_prod());
        }
        if(op2.getCont_res()!=null){
            op.setCont_res(op2.getCont_res());
        }
        if(op2.getDia_movi()!=null){
            op.setDia_movi(op2.getDia_movi());
        }
        if(op2.getEdad()!=null){
            op.setEdad(op2.getEdad());
        }
        if(op2.getLug_pro()!=null){
            op.setLug_pro(op2.getLug_pro());
        }
        if(op2.getMes_movi()!=null){
            op.setMes_movi(op2.getMes_movi());
        }
        if(op2.getCan_jefm()!=null){
            op.setCan_jefm(op2.getCan_jefm());
        }
        if(op2.getMot_viam()!=null){
            op.setMot_viam(op2.getMot_viam());
        }
        if(op2.getNac_migr()!=null){
            op.setNac_migr(op2.getNac_migr());
        }
        if(op2.getOcu_migr()!=null){
            op.setOcu_migr(op2.getOcu_migr());
        }
        if(op2.getPais_prod()!=null){
            op.setPais_prod(op2.getPais_prod());
        }
        if(op2.getPais_res()!=null){
            op.setPais_res(op2.getPais_res());
        }
        if(op2.getPro_jefm()!=null){
            op.setPro_jefm(op2.getPro_jefm());
        }
        if(op2.getSex_migr()!=null){
            op.setSex_migr(op2.getSex_migr());
        }
        if(op2.getSubcont_nac()!=null){
            op.setSubcont_nac(op2.getSubcont_nac());
        }
        if(op2.getSubcont_prod()!=null){
            op.setSubcont_prod(op2.getSubcont_prod());
        }
        if(op2.getTip_movi()!=null){
            op.setTip_movi(op2.getTip_movi());
        }
        if(op2.getTip_naci()!=null){
            op.setTip_naci(op2.getTip_naci());
        }
        if(op2.getVia_tran()!=null){
            op.setVia_tran(op2.getVia_tran());
        }
        list.remove(indice);
        list.insert(op, indice);
        list.getIndice(indice).toString();
        return true;
        
        
    
}

public int busquedaCedula(DoublyLinkedList<Persona> list, int c){
    int d=0;
    for(Persona op: list){
        
        if(op.getRegistro()==c){
            return d;
        }
        d++;
    }
    return d;
}

}