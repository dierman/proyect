    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.espol.edu.tdas;

/**
 *
 * @author luis8
 */
public interface List<E> {
    public abstract boolean  addFirst(E element);
    public abstract boolean addLast(E element);
    public abstract boolean removeFirst();
    public abstract boolean removeLast();
    public abstract boolean isEmpty();
    public abstract boolean contains(E element);
    public abstract E getIndice(int index);
    public abstract E getFirst();
    public abstract E getLast();
    public abstract int size();
    public abstract boolean remove(int index);
    public abstract boolean insert(E element, int index);
}
