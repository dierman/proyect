/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.espol.edu.tdas;

import java.util.Iterator;

/**
 *
 * @author luis8
 */
public class DoublyLinkedList<E> implements List<E>, Iterable<E> {
    private Node<E> first,last;
    private int efectivo;
    
    public DoublyLinkedList(){
        this.first=this.last=null;
        this.efectivo=0;
    }
    
    @Override
    public boolean isEmpty(){
        return (first==null && last==null);
    }
    
    @Override
    public boolean addFirst(E element){
        if(element==null){
            return false;
        }
        Node<E> nuevo= new Node<>(element);
        if(isEmpty()){
            this.first=this.last=nuevo;
            efectivo=efectivo+1;
            return true;

        }
        nuevo.setNext(this.first);
        this.first.setPrevious(nuevo);
        this.first=nuevo;
        efectivo=efectivo+1;
        return true;
        
    }
    
    @Override
    public boolean addLast(E element){
        if(element==null){
            return false;
        }
        Node<E> nuevo= new Node<>(element);
        if(isEmpty()){
            this.first=this.last=nuevo;
            efectivo=efectivo+1;
            return true;

        }
         this.last.setNext(nuevo);
         nuevo.setPrevious(this.last);
         this.last=nuevo;
         efectivo=efectivo+1;
         return true;
    }
    
    @Override
    public boolean removeFirst(){
        if(isEmpty()){
            return false;
        }else if(this.first==this.last){
            this.first=this.last=null;
            efectivo=efectivo-1;
            return true;
        }else{
            Node<E> temp=this.first;
            this.first=this.first.getNext();
            this.first.setPrevious(null);
            temp.setNext(null);
            efectivo=efectivo-1;
            return true;
        }
    }
    
   
    
    @Override
    public boolean removeLast(){
        if(isEmpty()){
            return false;
        }else if(this.first==this.last){
            this.first=this.last=null;
            efectivo=efectivo-1;
            return true;
        }else{
            Node<E> prev=this.last.getPrevious();
            this.last.setPrevious(null);
            prev.setNext(null);
            this.last=prev;
            efectivo=efectivo-1;
            return true;
        }
    }
    
    @Override
    public boolean contains(E element){  
        if(element==null || isEmpty()){
            return false;
        }else if(first==last && first.getData().equals(element)){
            return true;
        }
        Node<E> inicio=this.first;
        Node<E> fin=this.last;
        while((fin.getNext()!=inicio.getPrevious() || fin.getNext()==null || inicio.getPrevious()==null ) && fin.getNext()!=inicio){
            if(inicio.getData().equals(element) || fin.getData().equals(element) ){
                return true;
            }
            inicio=inicio.getNext();
            fin=fin.getPrevious();
        }
        return false;
    }
    
    @Override
    public E getIndice(int index){
        int cont=0;
        for(Node<E> p=this.first;p!=null;p=p.getNext()){
            if(cont==index){
                return p.getData();
            }
            cont++;
        }
        return null;
    }
    
    @Override
    public E getFirst(){
        if(isEmpty()){
            return null;
        }else{
        return this.first.getData();
        }
    }
    
    @Override
    public E getLast(){
        if(isEmpty()){
            return null;
        }else{
        return this.last.getData();
        }
    }
    
    @Override
    public int size(){
        return efectivo;
    }
    
    
    @Override
    public boolean remove(int index){
        int cont=0;
        for(Node<E> p=this.first;p!=null;p=p.getNext()){
            if(cont==index){
                if(p==this.first){
                    this.removeFirst();
                    return true;
                }else if(p==this.last){
                    this.removeLast();
                    return true;
                }else{
                    Node<E> prev=p.getPrevious();
                    Node<E> after=p.getNext();
                    p.setData(null);
                    p.setNext(null);
                    p.setPrevious(null);
                    after.setPrevious(prev);
                    prev.setNext(after);
                    efectivo=efectivo-1;
                    return true;
                 }
            }
            cont++;
        }
        return false;
    }
    
    @Override
    public boolean insert(E element, int index){
        int cont=0;
        if(element==null){
            return false;
        }
        if(index==this.efectivo){
            this.addLast(element);
            return true;
        }
        for(Node<E> p=this.first;p!=null;p=p.getNext()){
            if(cont==index){
                if(p==this.first){
                    this.addFirst(element);
                    return true;
                }else{
                    Node<E> prev=p.getPrevious();
                    Node<E> nuevo=new Node<>(element);
                    prev.setNext(nuevo);
                    nuevo.setPrevious(prev);
                    nuevo.setNext(p);
                    p.setPrevious(nuevo);
                    efectivo=efectivo+1;
                    return true;
                 }
            }
            cont++;
        }
        return false;
    }
    
    @Override
    public boolean equals(Object ob){
        if(ob==null || !(ob instanceof DoublyLinkedList )){
            return false;
        }else{
            DoublyLinkedList sll= (DoublyLinkedList) ob;
            if(sll.size()==this.size()){
                for(int i=0;i<this.size();i++){
                    if(this.getIndice(i)!=sll.getIndice(i)){
                        return false;
                    }
                }
                return true;
            }else{
                return false;
            }
            
        }
    }
    
    @Override
    public String toString(){
        StringBuilder sb=new StringBuilder();
        for(Node<E> p=this.first;p!=null;p=p.getNext()){
            if(p==this.first && p==this.last){
                sb.append("[");
                sb.append(this.first.getData());
                sb.append("]");
            }else if(p==this.first){
                sb.append("[");
                sb.append(this.first.getData());
                sb.append(",");
            }else if(p==this.last){
                sb.append(this.last.getData());
                sb.append("]");
            }else{
                sb.append(p.getData());
                sb.append(",");
            }
        }
        return sb.toString();
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E> i= new Iterator<E>(){
           private Node<E> p=first;
           
           @Override
           public boolean hasNext(){
               return p!=null;
           }
           
           @Override
           public E next(){
               E dato=p.getData();
               p=p.getNext();
               return dato;
           }
        
        };
                return i;
    }
}
