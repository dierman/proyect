/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.espol.edu.main;

import ec.espol.edu.tdas.DoublyLinkedList;
import ec.espol.edu.tdas.Operaciones;
import ec.espol.edu.tdas.Persona;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 *
 * @author diego
 */
public class Proyect extends Application {
    static Scene scene;
    static DoublyLinkedList<Persona> lista=new DoublyLinkedList<>();
    @Override
    public void start(Stage primaryStage) {
        Operaciones ope=new Operaciones();
        ope.leerArchivo(lista);
        
        Button btn = new Button();
        btn.setText("Agregar Registro");
        
                Button btn1 = new Button();
        btn1.setText("Modificar Registro");

                Button btn2 = new Button();
        btn2.setText("Eliminar Registro");
        
        Group root = new Group();
        btn.setLayoutX(150);
        btn.setLayoutY(120);
        btn.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
        btn1.setLayoutX(150);
        btn1.setLayoutY(210);
        btn1.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
        btn.setTextFill(Color.BLUE);
        btn1.setTextFill(Color.BLUE);
        btn2.setLayoutX(150);
        btn2.setLayoutY(300);
        btn2.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
        btn2.setTextFill(Color.BLUE);
        root.getChildren().add(btn);
        root.getChildren().add(btn1);
        root.getChildren().add(btn2);
        
        
        
     
        //FONDO
        Image imfondo = new Image("file:fondo1.png");
        ImageView iF = new ImageView(imfondo);
        iF.setFitHeight(450);
        iF.setFitWidth(450);
        iF.setLayoutX(0);
        iF.setLayoutY(0);
        root.getChildren().add(iF);
        iF.toBack();
        Scene scene = new Scene(root, 500, 450);
           primaryStage.setTitle("Proyecto");
        primaryStage.setScene(scene);
        primaryStage.show();
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                Group rootAgre=new Group();
                Label lab1 = new Label("tip_movi");
                TextField text1=new TextField();
                text1.setLayoutX(200);
                text1.setLayoutY(50);
                lab1.setTextFill(Color.WHITE);
                lab1.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                lab1.setLayoutX(50);
                lab1.setLayoutY(50);
                Label lab2 = new Label("tip_nac");
                TextField text2=new TextField();
                text2.setLayoutX(200);
                text2.setLayoutY(100);
                lab2.setLayoutX(50);
                lab2.setLayoutY(100);
                lab2.setTextFill(Color.WHITE);
                lab2.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab3 = new Label("via_tran");
                TextField text3=new TextField();
                text3.setLayoutX(200);
                text3.setLayoutY(150);
                lab3.setLayoutX(50);
                lab3.setLayoutY(150);
                lab3.setTextFill(Color.WHITE);
                lab3.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab4 = new Label("pro_jefm");
                TextField text4=new TextField();
                text4.setLayoutX(200);
                text4.setLayoutY(200);
                lab4.setLayoutX(50);
                lab4.setLayoutY(200);
                lab4.setTextFill(Color.WHITE);
                lab4.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab5 = new Label("can_jefm");
                TextField text5=new TextField();
                text5.setLayoutX(200);
                text5.setLayoutY(250);
                lab5.setLayoutX(50);
                lab5.setLayoutY(250);
                lab5.setTextFill(Color.WHITE);
                lab5.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab6 = new Label("ani_movi");
                TextField text6=new TextField();
                text6.setLayoutX(200);
                text6.setLayoutY(300);
                lab6.setLayoutX(50);
                lab6.setLayoutY(300);
                lab6.setTextFill(Color.WHITE);
                lab6.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab7 = new Label("mes_movi");
                TextField text7=new TextField();
                text7.setLayoutX(200);
                text7.setLayoutY(350);
                lab7.setLayoutX(50);
                lab7.setLayoutY(350);
                lab7.setTextFill(Color.WHITE);
                lab7.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab8 = new Label("dia_movi");
                TextField text8=new TextField();
                text8.setLayoutX(200);
                text8.setLayoutY(400);
                lab8.setLayoutX(50);
                lab8.setLayoutY(400);
                lab8.setTextFill(Color.WHITE);
                lab8.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab9 = new Label("sex_,migr");
                TextField text9=new TextField();
                text9.setLayoutX(500);
                text9.setLayoutY(50);
                lab9.setLayoutX(350);
                lab9.setLayoutY(50);
                lab9.setTextFill(Color.WHITE);
                lab9.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab10 = new Label("ani_nac");
                TextField text10=new TextField();
                text10.setLayoutX(500);
                text10.setLayoutY(100);
                lab10.setLayoutX(350);
                lab10.setLayoutY(100);
                lab10.setTextFill(Color.WHITE);
                lab10.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab11= new Label("ocu_migr");
                TextField text11=new TextField();
                text11.setLayoutX(500);
                text11.setLayoutY(150);
                lab11.setLayoutX(350);
                lab11.setLayoutY(150);
                lab11.setTextFill(Color.WHITE);
                lab11.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab12 = new Label("cla_migr");
                TextField text12=new TextField();
                text12.setLayoutX(500);
                text12.setLayoutY(200);
                lab12.setLayoutX(350);
                lab12.setLayoutY(200);
                lab12.setTextFill(Color.WHITE);
                lab12.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab13 = new Label("mot_viam");
                TextField text13=new TextField();
                text13.setLayoutX(500);
                text13.setLayoutY(250);
                lab13.setLayoutX(350);
                lab13.setLayoutY(250);
                lab13.setTextFill(Color.WHITE);
                lab13.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab14 = new Label("nac_migr");
                TextField text14=new TextField();
                text14.setLayoutX(500);
                text14.setLayoutY(300);
                lab14.setLayoutX(350);
                lab14.setLayoutY(300);
                lab14.setTextFill(Color.WHITE);
                lab14.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab15 = new Label("pais_prod");
                TextField text15=new TextField();
                text15.setLayoutX(500);
                text15.setLayoutY(350);
                lab15.setLayoutX(350);
                lab15.setLayoutY(350);
                lab15.setTextFill(Color.WHITE);
                lab15.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab16 = new Label("pais_red");
                TextField text16=new TextField();
                text16.setLayoutX(500);
                text16.setLayoutY(400);
                lab16.setLayoutX(350);
                lab16.setLayoutY(400);
                lab16.setTextFill(Color.WHITE);
                lab16.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab17 = new Label("lug_pro");
                TextField text17=new TextField();
                text17.setLayoutX(800);
                text17.setLayoutY(50);
                lab17.setLayoutX(650);
                lab17.setLayoutY(50);
                lab17.setTextFill(Color.WHITE);
                lab17.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab18 = new Label("edad");
                TextField text18=new TextField();
                text18.setLayoutX(800);
                text18.setLayoutY(100);
                lab18.setLayoutX(650);
                lab18.setLayoutY(100);
                lab18.setTextFill(Color.WHITE);
                lab18.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab19 = new Label("cont_prod");
                TextField text19=new TextField();
                text19.setLayoutX(800);
                text19.setLayoutY(150);
                lab19.setLayoutX(650);
                lab19.setLayoutY(150);
                lab19.setTextFill(Color.WHITE);
                lab19.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab20 = new Label("cont_res");
                TextField text20=new TextField();
                text20.setLayoutX(800);
                text20.setLayoutY(200);
                lab20.setLayoutX(650);
                lab20.setLayoutY(200);
                lab20.setTextFill(Color.WHITE);
                lab20.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab21 = new Label("cont_nac");
                TextField text21=new TextField();
                text21.setLayoutX(800);
                text21.setLayoutY(250);
                lab21.setLayoutX(650);
                lab21.setLayoutY(250);
                lab21.setTextFill(Color.WHITE);
                lab21.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab22 = new Label("subcont_proc");
                TextField text22=new TextField();
                text22.setLayoutX(800);
                text22.setLayoutY(300);
                lab22.setLayoutX(650);
                lab22.setLayoutY(300);
                lab22.setTextFill(Color.WHITE);
                lab22.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab23 = new Label("subcont_nac");
                TextField text23=new TextField();
                text23.setLayoutX(800);
                text23.setLayoutY(350);
                lab23.setLayoutX(650);
                lab23.setLayoutY(350);
                lab23.setTextFill(Color.WHITE);
                lab23.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Button menuP = new Button("Enviar");
                menuP.setTextFill(Color.DARKBLUE);
                menuP.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
                menuP.setLayoutX(600);
                menuP.setLayoutY(500);
                Button menuJ = new Button("Volver");
                menuJ.setTextFill(Color.DARKBLUE);
                menuJ.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
                menuJ.setLayoutX(900);
                menuJ.setLayoutY(500);
                
                rootAgre.getChildren().addAll(lab1,lab2,lab3,lab4,lab5,lab6,lab7,lab8,lab9,lab10,lab11,lab12,lab13,lab14,lab15,lab16,lab17,lab18,lab19,lab20,lab21,lab22,lab23,text1,text2,text3,text4,text5,text6,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20,text21,text22,text23,menuP,menuJ);
                Scene scene1=new Scene(rootAgre,1000,600);
                scene1.setFill(Color.GREEN);
                primaryStage.setTitle("Agregar Persona");
                primaryStage.setScene(scene1);
                menuJ.setOnAction((ActionEvent t)->{
                    primaryStage.setScene(scene);
                });
                menuP.setOnAction((ActionEvent t)->{
                    Persona per=new Persona(ope.ced_auto(),text1.getText(),text2.getText(),text3.getText(),text4.getText(),text5.getText(),text6.getText(),text7.getText(),text8.getText(),text9.getText(),text10.getText(),text11.getText(),text12.getText(),text13.getText(),text14.getText(),text15.getText(),text16.getText(),text17.getText(),text18.getText(),text19.getText(),text20.getText(),text21.getText(),text22.getText(),text23.getText());
                    if(ope.agregarPersona(lista, per)){
                        Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Agregado");
 
        
                    alert.setHeaderText(null);
                    alert.setContentText("La persona fue agregada exitosamente");
 
                    alert.showAndWait();}
                    
                    
                });
                
            }
        });
        btn2.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                Group rootElim=new Group();
                Label lab1 = new Label("INGRESE EL NUMERO DE REGISTRO \n DE LA PERSONA QUE DESEE AGREGAR");
                TextField text1=new TextField();
                text1.setLayoutX(200);
                text1.setLayoutY(200);
                lab1.setTextFill(Color.WHITE);
                lab1.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                lab1.setLayoutX(100);
                lab1.setLayoutY(100);
                 Button menuP = new Button("Eliminar");
                menuP.setTextFill(Color.DARKBLUE);
                menuP.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
                menuP.setLayoutX(100);
                menuP.setLayoutY(400);
                Button menuJ = new Button("Volver");
                menuJ.setTextFill(Color.DARKBLUE);
                menuJ.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
                menuJ.setLayoutX(300);
                menuJ.setLayoutY(400);
                
                rootElim.getChildren().addAll(lab1,text1,menuP,menuJ);
                Scene scene2=new Scene(rootElim,500,600);
                scene2.setFill(Color.GREEN);
                primaryStage.setTitle("Eliminar Persona");
                primaryStage.setScene(scene2);
                menuJ.setOnAction((ActionEvent t)->{
                    primaryStage.setScene(scene);
                });
                menuP.setOnAction((ActionEvent t)->{
                    if(ope.eliminarPersona(lista, Integer.parseInt(text1.getText()))){
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Eliminado");
 
        
                    alert.setHeaderText(null);
                    alert.setContentText("La persona fue eliminada exitosamente");
 
                    alert.showAndWait();}
                    else{
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error");
 
        
                    alert.setHeaderText(null);
                    alert.setContentText("La persona no existe");
 
                    alert.showAndWait();}    
                    });
                
            }
        });
                btn1.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
            public void handle(ActionEvent event) {
            Group rootAgre=new Group();
                Label lab = new Label("INGRESE EL NUMERO DE REGISTRO \n DE LA PERSONA QUE DESEA \n MODIFICAR DATOS");
                TextField text=new TextField();
                text.setLayoutX(400);
                text.setLayoutY(500);
                lab.setTextFill(Color.WHITE);
                lab.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                lab.setLayoutX(50);
                lab.setLayoutY(500);
                Label lab1 = new Label("tip_movi");
                TextField text1=new TextField();
                text1.setLayoutX(200);
                text1.setLayoutY(50);
                lab1.setTextFill(Color.WHITE);
                lab1.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                lab1.setLayoutX(50);
                lab1.setLayoutY(50);
                Label lab2 = new Label("tip_nac");
                TextField text2=new TextField();
                text2.setLayoutX(200);
                text2.setLayoutY(100);
                lab2.setLayoutX(50);
                lab2.setLayoutY(100);
                lab2.setTextFill(Color.WHITE);
                lab2.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab3 = new Label("via_tran");
                TextField text3=new TextField();
                text3.setLayoutX(200);
                text3.setLayoutY(150);
                lab3.setLayoutX(50);
                lab3.setLayoutY(150);
                lab3.setTextFill(Color.WHITE);
                lab3.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab4 = new Label("pro_jefm");
                TextField text4=new TextField();
                text4.setLayoutX(200);
                text4.setLayoutY(200);
                lab4.setLayoutX(50);
                lab4.setLayoutY(200);
                lab4.setTextFill(Color.WHITE);
                lab4.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab5 = new Label("can_jefm");
                TextField text5=new TextField();
                text5.setLayoutX(200);
                text5.setLayoutY(250);
                lab5.setLayoutX(50);
                lab5.setLayoutY(250);
                lab5.setTextFill(Color.WHITE);
                lab5.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab6 = new Label("ani_movi");
                TextField text6=new TextField();
                text6.setLayoutX(200);
                text6.setLayoutY(300);
                lab6.setLayoutX(50);
                lab6.setLayoutY(300);
                lab6.setTextFill(Color.WHITE);
                lab6.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab7 = new Label("mes_movi");
                TextField text7=new TextField();
                text7.setLayoutX(200);
                text7.setLayoutY(350);
                lab7.setLayoutX(50);
                lab7.setLayoutY(350);
                lab7.setTextFill(Color.WHITE);
                lab7.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab8 = new Label("dia_movi");
                TextField text8=new TextField();
                text8.setLayoutX(200);
                text8.setLayoutY(400);
                lab8.setLayoutX(50);
                lab8.setLayoutY(400);
                lab8.setTextFill(Color.WHITE);
                lab8.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab9 = new Label("sex_,migr");
                TextField text9=new TextField();
                text9.setLayoutX(500);
                text9.setLayoutY(50);
                lab9.setLayoutX(350);
                lab9.setLayoutY(50);
                lab9.setTextFill(Color.WHITE);
                lab9.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab10 = new Label("ani_nac");
                TextField text10=new TextField();
                text10.setLayoutX(500);
                text10.setLayoutY(100);
                lab10.setLayoutX(350);
                lab10.setLayoutY(100);
                lab10.setTextFill(Color.WHITE);
                lab10.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab11= new Label("ocu_migr");
                TextField text11=new TextField();
                text11.setLayoutX(500);
                text11.setLayoutY(150);
                lab11.setLayoutX(350);
                lab11.setLayoutY(150);
                lab11.setTextFill(Color.WHITE);
                lab11.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab12 = new Label("cla_migr");
                TextField text12=new TextField();
                text12.setLayoutX(500);
                text12.setLayoutY(200);
                lab12.setLayoutX(350);
                lab12.setLayoutY(200);
                lab12.setTextFill(Color.WHITE);
                lab12.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab13 = new Label("mot_viam");
                TextField text13=new TextField();
                text13.setLayoutX(500);
                text13.setLayoutY(250);
                lab13.setLayoutX(350);
                lab13.setLayoutY(250);
                lab13.setTextFill(Color.WHITE);
                lab13.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab14 = new Label("nac_migr");
                TextField text14=new TextField();
                text14.setLayoutX(500);
                text14.setLayoutY(300);
                lab14.setLayoutX(350);
                
                lab14.setLayoutY(300);
                lab14.setTextFill(Color.WHITE);
                lab14.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab15 = new Label("pais_prod");
                TextField text15=new TextField();
                text15.setLayoutX(500);
                text15.setLayoutY(350);
                lab15.setLayoutX(350);
                lab15.setLayoutY(350);
                lab15.setTextFill(Color.WHITE);
                lab15.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab16 = new Label("pais_red");
                TextField text16=new TextField();
                text16.setLayoutX(500);
                text16.setLayoutY(400);
                lab16.setLayoutX(350);
                lab16.setLayoutY(400);
                lab16.setTextFill(Color.WHITE);
                lab16.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab17 = new Label("lug_pro");
                TextField text17=new TextField();
                text17.setLayoutX(800);
                text17.setLayoutY(50);
                lab17.setLayoutX(650);
                lab17.setLayoutY(50);
                lab17.setTextFill(Color.WHITE);
                lab17.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab18 = new Label("edad");
                TextField text18=new TextField();
                text18.setLayoutX(800);
                text18.setLayoutY(100);
                lab18.setLayoutX(650);
                lab18.setLayoutY(100);
                lab18.setTextFill(Color.WHITE);
                lab18.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab19 = new Label("cont_prod");
                TextField text19=new TextField();
                text19.setLayoutX(800);
                text19.setLayoutY(150);
                lab19.setLayoutX(650);
                lab19.setLayoutY(150);
                lab19.setTextFill(Color.WHITE);
                lab19.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab20 = new Label("cont_res");
                TextField text20=new TextField();
                text20.setLayoutX(800);
                text20.setLayoutY(200);
                lab20.setLayoutX(650);
                lab20.setLayoutY(200);
                lab20.setTextFill(Color.WHITE);
                lab20.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab21 = new Label("cont_nac");
                TextField text21=new TextField();
                text21.setLayoutX(800);
                text21.setLayoutY(250);
                lab21.setLayoutX(650);
                lab21.setLayoutY(250);
                lab21.setTextFill(Color.WHITE);
                lab21.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab22 = new Label("subcont_proc");
                TextField text22=new TextField();
                text22.setLayoutX(800);
                text22.setLayoutY(300);
                lab22.setLayoutX(650);
                lab22.setLayoutY(300);
                lab22.setTextFill(Color.WHITE);
                lab22.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Label lab23 = new Label("subcont_nac");
                TextField text23=new TextField();
                text23.setLayoutX(800);
                text23.setLayoutY(350);
                lab23.setLayoutX(650);
                lab23.setLayoutY(350);
                lab23.setTextFill(Color.WHITE);
                lab23.setFont(Font.font("Calibri",FontWeight.BOLD,20));
                Button menuP = new Button("Enviar");
                menuP.setTextFill(Color.DARKBLUE);
                menuP.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
                menuP.setLayoutX(600);
                menuP.setLayoutY(500);
                Button menuJ = new Button("Volver");
                menuJ.setTextFill(Color.DARKBLUE);
                menuJ.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
                menuJ.setLayoutX(900);
                menuJ.setLayoutY(500);
                
                rootAgre.getChildren().addAll(lab1,lab2,lab3,lab4,lab5,lab6,lab7,lab8,lab9,lab10,lab11,lab12,lab13,lab14,lab15,lab16,lab17,lab18,lab19,lab20,lab21,lab22,lab23,text1,text2,text3,text4,text5,text6,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20,text21,text22,text23,menuP,menuJ,text,lab);
                Scene scene1=new Scene(rootAgre,1000,600);
                scene1.setFill(Color.GREEN);
                primaryStage.setTitle("Modificar persona");
                primaryStage.setScene(scene1);
                menuJ.setOnAction((ActionEvent t)->{
                    primaryStage.setScene(scene);
                });
                menuP.setOnAction((ActionEvent t)->{
                    Persona per=new Persona(Integer.parseInt(text.getText()),text1.getText(),text2.getText(),text3.getText(),text4.getText(),text5.getText(),text6.getText(),text7.getText(),text8.getText(),text9.getText(),text10.getText(),text11.getText(),text12.getText(),text13.getText(),text14.getText(),text15.getText(),text16.getText(),text17.getText(),text18.getText(),text19.getText(),text20.getText(),text21.getText(),text22.getText(),text23.getText());
                    if(ope.modificarPersona(lista,Integer.parseInt(text.getText()), per)){
                       Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Modificacion");
 
        
                    alert.setHeaderText(null);
                    alert.setContentText("La persona fue modificada exitosamente");
 
                    alert.showAndWait();}
                    
                });
                
            }
        });
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
